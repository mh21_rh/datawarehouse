[metadata]
name = datawarehouse
description = DataWarehouse for CKI pipelines
author = CKI Project
license = GPLv2+
version = 1

[options]
# Automatically find all files beneath the kpet directory and include them.
packages = datawarehouse
# Parse the MANIFEST.in file and include those files, too.
include_package_data = True
scripts =
    manage.py
    entrypoint.sh
install_requires =
    celery
    cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git@production
    crontab
    Django
    django-cors-headers # For frontend devel only
    django-debug-toolbar
    django-extensions
    django-prometheus
    django-simple-captcha
    djangorestframework
    djangosaml2
    ipython
    mozilla_django_oidc
    natsort
    ldap3
    pika # rabbitmq
    psycopg2-binary
    requests-futures # For patches downloading
    sentry-sdk
    uwsgi
    pymemcache

[options.extras_require]
test =
    curlylint
    pytest<8.2.0
    responses
    pylint-django
    pytest-django
    freezegun
    pylint
dev =
    pympler # For memory profiling
    tox
    datawarehouse[test]

[testenv]
passenv =
    DB_HOST
    DB_PASSWORD
    SECRET_KEY
    GITLAB_URL
    BEAKER_URL
    CKI_DEPLOYMENT_ENVIRONMENT
setenv =
    FF_SIGNUP_ENABLED=true
    CAPTCHA_TEST_MODE=true
    # Tests can't run with DEBUG=True, make sure settings comply
    # https://docs.djangoproject.com/en/4.1/topics/testing/overview/#other-test-conditions
    DEBUG=False
extras = test
commands = python3 manage.py check
           python3 manage.py makemigrations --check
           cki_lint.sh datawarehouse
           python3 -m curlylint datawarehouse/templates/
           cki_test.sh datawarehouse

[testenv:lint]
commands = python3 manage.py check
           python3 -m curlylint datawarehouse/templates/
           cki_lint.sh datawarehouse

[testenv:test]
commands = python3 manage.py check
           python3 manage.py makemigrations --check
           cki_test.sh datawarehouse

[tool:pytest]
DJANGO_SETTINGS_MODULE = datawarehouse.settings
addopts = --ignore=datawarehouse/models --ignore=db --doctest-modules
doctest_optionflags = NORMALIZE_WHITESPACE

[pylint.MAIN]
disable=duplicate-code,cyclic-import,too-many-ancestors
load-plugins=pylint_django
django-settings-module = datawarehouse.settings
ignore=migrations
# After the upgrade to Fedora 37 in the gitlab runners, pylint has
# started reporting `no method` errors in function calls of these
# modules. Let's disable those checks in these modules until the problem
# is solved.
generated-members=os.*
ignored-modules=collections.abc*
allowed-redefined-builtins = id
good-names = id, i

[flake8]
# Django recommended
max-line-length = 119
per-file-ignores =
    datawarehouse/models/*.py:DJ01,DJ09
    datawarehouse/models/__init__.py:F401
    datawarehouse/scripts/__init__.py:F401
    **/urls.py:DJ05

[FORMAT]
max-line-length=119

[coverage:run]
omit =
    datawarehouse/migrations/*

[coverage:report]
exclude_also =
    if settings.DEBUG
    if DEBUG
    if typing.TYPE_CHECKING:
    raise NotImplementedError

[tox:tox]
