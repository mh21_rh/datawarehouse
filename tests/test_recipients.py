"""Tests for recipient parsing."""
from unittest import mock

from django import test

from datawarehouse import models
from datawarehouse import recipients


class TestRecipientsMethods(test.TestCase):
    """Tests for recipient parsing."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/checkouts_and_recipients.yaml',
    ]

    def test_when_always(self):
        """Test the when_always function."""
        self.assertTrue(recipients.when_always(None))

    def test_when_failed_regarding_checkout(self):
        """Test the when_failed function regarding checkout."""
        checkout_queryset = models.KCIDBCheckout.objects.filter(id="redhat:1")

        cases = [
            # patched_attrs, expected result
            ({"valid": True}, {"when_failed": False, "when_failed_tests": False}),
            ({"valid": False}, {"when_failed": True, "when_failed_tests": False}),
        ]

        for patched_attrs, expected in cases:
            with self.subTest(**patched_attrs):
                checkout_queryset.update(**patched_attrs)
                checkout = checkout_queryset.aggregated().get()  # Refresh aggregated()

                self.assertEqual(bool(recipients.when_failed(checkout)), expected["when_failed"])
                self.assertEqual(bool(recipients.when_failed_tests(checkout)), expected["when_failed_tests"])

    def test_when_failed_regarding_build(self):
        """Test the when_failed function regarding build."""
        checkout_queryset = models.KCIDBCheckout.objects.filter(id="redhat:1")
        build_queryset = models.KCIDBBuild.objects.filter(id="redhat:887318")

        cases = [
            # patched_attrs, expected result
            ({"valid": True}, {"when_failed": False, "when_failed_tests": False}),
            ({"valid": False}, {"when_failed": True, "when_failed_tests": False}),
        ]

        for patched_attrs, expected in cases:
            with self.subTest(**patched_attrs):
                build_queryset.update(**patched_attrs)
                checkout = checkout_queryset.aggregated().get()  # Refresh aggregated()

                self.assertEqual(bool(recipients.when_failed(checkout)), expected["when_failed"])
                self.assertEqual(bool(recipients.when_failed_tests(checkout)), expected["when_failed_tests"])

    def test_when_failed_regarding_test(self):
        """Test the when_failed function regarding test."""
        checkout = models.KCIDBCheckout.objects.get()
        self.assertTrue(
            checkout.valid and checkout.all_builds_passed,
            "Precondition: checkout and it's builds are ok",
        )

        cases = [
            (
                "No tests",
                models.KCIDBTest.objects.filter(test__name__in=["No match"]),
                {"when_failed": False, "when_failed_tests": False},
            ),
            (
                "One test",
                models.KCIDBTest.objects.filter(test__name__in=["Test 1"]),
                {"when_failed": True, "when_failed_tests": True},
            ),
            (
                "Multiple tests",
                models.KCIDBTest.objects.all(),
                {"when_failed": True, "when_failed_tests": True},
            ),
        ]
        for description, tests, expected in cases:
            with (
                self.subTest(description),
                mock.patch.object(models.KCIDBCheckout, "untriaged_tests_or_regression_blocking", tests),
            ):
                # `all_builds_passed + has_untriaged_tests_or_regression_blocking`
                with self.assertNumQueries(2):
                    when_failed = bool(recipients.when_failed(checkout))

                # just `has_untriaged_tests_or_regression_blocking`
                with self.assertNumQueries(1):
                    when_failed_tests = bool(recipients.when_failed_tests(checkout))

                result = {"when_failed": when_failed, "when_failed_tests": when_failed_tests}
                self.assertEqual(result, expected)

    def test_failed_test_maintainers(self):
        """Test failed_tests_maintainers function."""
        checkout = models.KCIDBCheckout.objects.get()

        cases = [
            (
                "No tests",
                models.KCIDBTest.objects.filter(test__name__in=["No match"]),
                [],
            ),
            (
                "One test without maintainers",
                models.KCIDBTest.objects.filter(test__name__in=["Test 3"]),
                [],
            ),
            (
                "one test with maintainers",
                models.KCIDBTest.objects.filter(test__name__in=["Test 1"]),
                ["maint1@email.com"],
            ),
            (
                "Multiple tests with maintainers",
                models.KCIDBTest.objects.all(),
                ["maint1@email.com", "maint2@email.com"],
            ),
        ]
        for description, tests, expected_maintainers in cases:
            with (
                self.subTest(description),
                mock.patch.object(models.KCIDBCheckout, "untriaged_tests_or_regression_blocking", tests),
            ):
                with self.assertNumQueries(1):
                    result = recipients.failed_tests_maintainers(checkout)  # Call!

                self.assertEqual(result, expected_maintainers)

    def test_submitter(self):
        """Test submitter function."""
        checkout = models.KCIDBCheckout.objects.get()
        self.assertEqual(recipients.submitter(checkout), [])

        checkout.submitter = models.Maintainer.create_from_address('foo@bar.com')
        self.assertEqual(recipients.submitter(checkout), ['foo@bar.com'])


class TestRecipients(test.TestCase):
    """Tests for recipient parsing."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/checkouts_and_recipients.yaml',
    ]

    def test_recipients_keywords(self):
        """Test recipients_keywords content."""
        self.assertEqual(
            recipients.Recipients.recipients_keywords,
            {
                'failed_tests_maintainers': recipients.failed_tests_maintainers,
                'submitter': recipients.submitter,
            }
        )

    def test_evaluation_rules(self):
        """Test evaluation_rules content."""
        self.assertEqual(
            recipients.Recipients.evaluation_rules,
            {
                "always": recipients.when_always,
                "failed": recipients.when_failed,
                "failed_tests": recipients.when_failed_tests,
            },
        )

    def test_parse_recipients(self):
        """Test _parse_recipients function."""
        cases = [
            ('someone@mail.com', ['someone@mail.com']),
            (['someone@mail.com'], ['someone@mail.com']),
            ('failed_tests_maintainers', ['1@m.com', '2@m.com']),
            (['someone@mail.com', 'failed_tests_maintainers'],
             ['someone@mail.com', '1@m.com', '2@m.com']),
        ]

        recipients_keywords = {'failed_tests_maintainers': mock.Mock(return_value=['1@m.com', '2@m.com'])}
        with mock.patch('datawarehouse.recipients.Recipients.recipients_keywords', recipients_keywords):
            recipient = recipients.Recipients(mock.Mock())
            for recipients_input, expected_output in cases:
                self.assertEqual(
                    set(recipient._parse_recipients(recipients_input)),  # pylint: disable=protected-access
                    set(expected_output)
                )

    def test_conditions_match(self):
        """Test _conditions_match function."""
        # pylint: disable=protected-access
        evaluation_rules = {'foobar': mock.Mock(return_value=True)}
        with mock.patch('datawarehouse.recipients.Recipients.evaluation_rules', evaluation_rules):
            recipient = recipients.Recipients(mock.Mock())
            self.assertTrue(recipient._conditions_match({'when': 'foobar'}))
            self.assertTrue(evaluation_rules['foobar'].called)

            self.assertRaises(KeyError, recipient._conditions_match, {'when': 'missing-rule'})

    def test_render(self):
        """Test render returns the correct values."""
        rules = [
            {'when': 'always', 'send_to': ['a@a.com']},
            {'when': 'always', 'send_to': ['b@b.com'], 'report_template': 'long'},
            {'when': 'always', 'send_to': ['c@c.com', 'd@d.com'], 'send_bcc': ['e@e.com']},
            {'when': 'always', 'send_bcc': 'f@f.com'},
            {'when': 'always', 'send_to': 'failed_tests_maintainers', 'report_template': 'short'},
            {'when': 'always', 'send_to': ['c@c.com'], 'report_template': 'long'},
        ]
        models.KCIDBTest.objects.update(status='F', waived=False)
        models.KCIDBTestResult.objects.update(status="F")
        checkout = models.KCIDBCheckout.objects.get()
        checkout.report_rules = rules

        recipient = recipients.Recipients(checkout)

        self.assertEqual(
            recipient.render(),
            {
                'default': {'To': {'a@a.com', 'd@d.com'}, 'BCC': {'e@e.com', 'f@f.com'}},
                'short': {'To': {'maint1@email.com', 'maint2@email.com'}},
                'long': {'To': {'b@b.com', 'c@c.com'}},
            }
        )

    def test_render_conditions_fail(self):
        """Test render returns the correct values when some of the "when" conditions fail."""
        rules = [
            {'when': 'always', 'send_to': ['a@a.com']},
            {'when': 'always', 'send_to': ['b@b.com'], 'report_template': 'short'},
            {'when': 'failed', 'send_to': ['b@b.com', 'c@c.com'], 'report_template': 'long'},
        ]
        checkout = models.KCIDBCheckout.objects.aggregated().get()
        checkout.report_rules = rules

        recipient = recipients.Recipients(checkout)

        self.assertEqual(
            recipient.render(),
            {
                'default': {'To': {'a@a.com'}},
                'short': {'To': {'b@b.com'}},
            }
        )
