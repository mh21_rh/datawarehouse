"""Test for issues API."""
import json

from rest_framework.renderers import JSONRenderer

from datawarehouse import models
from datawarehouse import serializers
from tests import utils


class TestCheckoutIssues(utils.TestCase):
    """Test issues in checkouts."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]
    _obj = models.KCIDBCheckout
    _id = "redhat:public_checkout"
    _kcidb_endpoint = "kcidb/checkouts/{self._id}"
    permission_name = 'kcidbcheckout'
    related_name = 'kcidb_checkout'

    @property
    def obj(self):
        """Return the tested object."""
        return self._obj.objects.get(id=self._id)

    @property
    def kcidb_endpoint(self):
        """Return the endpoint formatted."""
        return self._kcidb_endpoint.format(self=self)

    def setUp(self):
        """Set up data."""
        self.issue_1 = models.Issue.objects.get(id=1)
        self.issue_2 = models.Issue.objects.get(id=2)

    def test_create_issues(self):
        """Test adding an issue."""
        response = self.assert_authenticated_post(
            201, f'add_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues',
            json.dumps({'issue_id': self.issue_1.id}), content_type="application/json")

        expected = json.loads(JSONRenderer().render(serializers.IssueSerializer(self.issue_1).data))
        self.assertDictEqual(expected, response.json())

        self.assertEqual(1, self.obj.issues.count())
        self.assertEqual(self.issue_1.id, self.obj.issues.get().id)

        response = self.assert_authenticated_post(
            201, f'add_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues',
            json.dumps({'issue_id': self.issue_2.id}), content_type="application/json")

        self.assertEqual(2, self.obj.issues.count())

    def test_get_issue_single(self):
        """Test getting an issue."""
        self.obj.issues.add(self.issue_1)
        self.assertEqual(1, self.obj.issues.count())

        response_single = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues/{self.issue_1.id}',
        )
        expected = json.loads(JSONRenderer().render(serializers.IssueSerializer(self.issue_1).data))
        self.assertEqual(expected, response_single.json())

    def test_get_issue_list(self):
        """Test getting issues."""
        self.obj.issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.issues.count())

        response_list = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues',
        )
        expected = json.loads(JSONRenderer().render(
            serializers.IssueSerializer([self.issue_2, self.issue_1], many=True).data
        ))
        self.assertEqual(expected, response_list.json()['results'])

    def test_delete_issues(self):
        """Test adding an issue."""
        self.obj.issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.issues.count())

        self.assert_authenticated_delete(
            204, f'delete_{self.permission_name}',
            f'/api/1/{self.kcidb_endpoint}/issues/{self.issue_1.id}',
            content_type="application/json")

        self.assertEqual(1, self.obj.issues.count())
        self.assertEqual(self.issue_2.id, self.obj.issues.get().id)

    def test_get_issue_occurrence_single(self):
        """Test getting an issue occurrence."""
        self.obj.issues.add(self.issue_1)
        self.assertEqual(1, self.obj.issues.count())

        response_single = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues/occurrences/{self.issue_1.id}',
        )
        expected = json.loads(JSONRenderer().render(
            serializers.IssueOccurrenceSerializer(
                models.IssueOccurrence.objects.get(**{
                    'issue': self.issue_1,
                    f'{self.related_name}__id': self.obj.id
                })).data
            )
        )
        self.assertEqual(expected, response_single.json())

    def test_get_issue_occurrence_list(self):
        """Test getting issue occurrences."""
        self.obj.issues.add(self.issue_1, self.issue_2)
        self.assertEqual(2, self.obj.issues.count())

        response_list = self.client.get(
            f'/api/1/{self.kcidb_endpoint}/issues/occurrences',
        )
        expected = json.loads(JSONRenderer().render(
            serializers.IssueOccurrenceSerializer(
                models.IssueOccurrence.objects.filter(**{
                    'issue__in': [self.issue_2, self.issue_1],
                    f'{self.related_name}__id': self.obj.id
                }), many=True).data)
        )

        self.assertEqual(response_list.json()['results'], expected)


class TestBuildIssues(TestCheckoutIssues):
    """Test issues in builds."""

    _obj = models.KCIDBBuild
    _id = "redhat:public_build_1"
    _kcidb_endpoint = "kcidb/builds/{self._id}"
    permission_name = 'kcidbbuild'
    related_name = 'kcidb_build'


class TestTestIssues(TestCheckoutIssues):
    """Test issues in tests."""

    _obj = models.KCIDBTest
    _id = "redhat:public_test_F"
    _kcidb_endpoint = "kcidb/tests/{self._id}"
    permission_name = 'kcidbtest'
    related_name = 'kcidb_test'


class TestTestResultIssues(TestCheckoutIssues):
    """Test issues in test results."""

    _obj = models.KCIDBTestResult
    _id = "redhat:public_test_F_result_F"
    _kcidb_endpoint = "kcidb/testresults/{self._id}"
    permission_name = 'kcidbtestresult'
    related_name = 'kcidb_testresult'
