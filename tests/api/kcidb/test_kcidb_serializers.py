"""Test serializers for KCIDB models."""
import json

from cki_lib import misc
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import validate_extended_kcidb_schema
from django.core.serializers.json import DjangoJSONEncoder

from datawarehouse.api.kcidb import serializers
from tests import utils


class BaseKCIDBSerializerTest(utils.TestCase):
    """Abstract class with common code for testing serializers for KCIDB models"""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_complete.yaml',
    ]

    path_to_expected_serialization = None
    serializer = None

    def __init__(self, *args, **kwargs):
        assert self.path_to_expected_serialization is not None
        assert self.serializer is not None

        super().__init__(*args, **kwargs)

    def setUp(self):
        with open(self.path_to_expected_serialization, "r", encoding="utf-8") as f:
            self.expected_serialization = json.load(f)

    def _assert_serializes_as_expected(self):
        queryset = self.serializer.Meta.model.objects.all()

        serializer = self.serializer(queryset, many=True)  # pylint: disable=not-callable
        json_serialized = json.dumps(serializer.data, cls=DjangoJSONEncoder, indent=2)
        try:
            self.assertJSONEqual(json_serialized, self.expected_serialization)
        except AssertionError:
            print(f"\nExpected {self.path_to_expected_serialization!r} to equal:\n{json_serialized}")
            raise

        return json_serialized

    def _assert_validate_extended_kcidb_schema(self, obj_type, json_serialized):
        """Test whether the serialized fixture"""
        serialized_data = json.loads(json_serialized)
        version = misc.get_nested_key(serialized_data[0], 'misc/kcidb/version')

        payload = {
            "version": version,
            obj_type: serialized_data
        }

        try:
            validate_extended_kcidb_schema(payload)
        except ValidationError as error:
            raise AssertionError(
                "Expected serialized data to validate against CKI's extended KCIDB schema."
            ) from error


class KCIDBCheckoutSerializerTest(BaseKCIDBSerializerTest):
    """Test KCIDBCheckoutSerializer"""

    serializer = serializers.KCIDBCheckoutSerializer
    path_to_expected_serialization = "tests/api/kcidb/fixtures/kcidbcheckoutserializer.json"

    def test_serialization(self):
        """Assert KCIDBCheckoutSerializer serializes KCIDBCheckout as expected"""
        json_serialized = self._assert_serializes_as_expected()
        self._assert_validate_extended_kcidb_schema("checkouts", json_serialized)


class KCIDBBuildSerializerTest(BaseKCIDBSerializerTest):
    """Test KCIDBBuildSerializer"""

    serializer = serializers.KCIDBBuildSerializer
    path_to_expected_serialization = "tests/api/kcidb/fixtures/kcidbbuildserializer.json"

    def test_serialization(self):
        """Assert KCIDBBuildSerializer serializes KCIDBBuild as expected"""
        json_serialized = self._assert_serializes_as_expected()
        self._assert_validate_extended_kcidb_schema("builds", json_serialized)


class KCIDBTestSerializerTest(BaseKCIDBSerializerTest):
    """Test KCIDBTestSerializer"""

    serializer = serializers.KCIDBTestSerializer
    path_to_expected_serialization = "tests/api/kcidb/fixtures/kcidbtestserializer.json"

    def test_serialization(self):
        """Assert KCIDBTestSerializer serializes KCIDBTest as expected"""
        json_serialized = self._assert_serializes_as_expected()
        self._assert_validate_extended_kcidb_schema("tests", json_serialized)


class KCIDBTestResultSerializerTest(BaseKCIDBSerializerTest):
    """Test KCIDBTestResultSerializer"""

    serializer = serializers.KCIDBTestResultSerializer
    path_to_expected_serialization = "tests/api/kcidb/fixtures/kcidbtestresultserializer.json"

    def test_serialization(self):
        """Assert KCIDBTestResultSerializer serializes KCIDBTestResult as expected"""
        self._assert_serializes_as_expected()


class KCIDBTestLightSerializerTest(BaseKCIDBSerializerTest):
    """Test KCIDBTestLightSerializer"""

    serializer = serializers.KCIDBTestLightSerializer
    path_to_expected_serialization = "tests/api/kcidb/fixtures/kcidbtestlightserializer.json"

    def test_serialization(self):
        """Assert KCIDBTestLightSerializer serializes KCIDBTest as expected"""
        self._assert_serializes_as_expected()


class KCIDBTestResultLightSerializerTest(BaseKCIDBSerializerTest):
    """Test KCIDBTestResultLightSerializer"""

    serializer = serializers.KCIDBTestResultLightSerializer
    path_to_expected_serialization = "tests/api/kcidb/fixtures/kcidbtestresultlightserializer.json"

    def test_serialization(self):
        """Assert KCIDBTestResultLightSerializer serializes KCIDBTestResult as expected"""
        self._assert_serializes_as_expected()
