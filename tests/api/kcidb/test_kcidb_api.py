"""Test the views module."""
from django.contrib.auth import get_user_model

from datawarehouse.api.kcidb import serializers
from datawarehouse.models import kcidb_models
from tests import utils

User = get_user_model()


class TestKCIDBCheckoutAPIAnonymous(utils.KCIDBTestCase):
    """Tests for the KCIDBCheckout endpoints."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/multiple_checkout_package_names.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml'
    ]
    anonymous = True
    groups = []

    def test_list(self):
        """Test list view of checkouts."""
        self._ensure_test_conditions('read')
        authorized_checkouts = kcidb_models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        response = self.client.get('/api/1/kcidb/checkouts')

        self.assertEqual(
            response.json()['results'],
            serializers.KCIDBCheckoutSerializer(authorized_checkouts, many=True).data,
        )

    def test_search_nvr(self):
        """Test searching based on nvr."""
        self._ensure_test_conditions('read')
        authorized_checkouts = kcidb_models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        ).annotation_for_filter()

        response_kernel = self.client.get('/api/1/kcidb/checkouts?nvr=kernel')
        filtered_checkouts = authorized_checkouts.filter(nvr__icontains='kernel')
        self.assertEqual(
            response_kernel.json()['results'],
            serializers.KCIDBCheckoutSerializer(filtered_checkouts, many=True).data,
        )

        response_kernel_rt = self.client.get('/api/1/kcidb/checkouts?nvr=rt-1.2.3')
        filtered_checkouts = authorized_checkouts.filter(nvr__icontains='rt-1.2.3')
        self.assertEqual(
            response_kernel_rt.json()['results'],
            serializers.KCIDBCheckoutSerializer(filtered_checkouts, many=True).data,
        )

        self.assertNotEqual(
            response_kernel.json()['results'],
            response_kernel_rt.json()['results'],
        )

    def test_search_id(self):
        """Test searching based on id."""
        self._ensure_test_conditions('read')
        authorized_checkouts = kcidb_models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        ).annotation_for_filter()

        response_kernel = self.client.get('/api/1/kcidb/checkouts?id=redhat:public_checkout_7')
        filtered_checkouts = authorized_checkouts.filter(id__icontains='redhat:public_checkout_7')
        self.assertEqual(
            response_kernel.json()['results'],
            serializers.KCIDBCheckoutSerializer(filtered_checkouts, many=True).data,
        )

    def test_search_origin(self):
        """Test searching based on origin."""
        self._ensure_test_conditions('read')
        authorized_checkouts = kcidb_models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        ).annotation_for_filter()

        response_kernel = self.client.get('/api/1/kcidb/checkouts?origin=tahder')
        filtered_checkouts = authorized_checkouts.filter(origin__name__icontains='tahder')
        self.assertEqual(
            response_kernel.json()['results'],
            serializers.KCIDBCheckoutSerializer(filtered_checkouts, many=True).data,
        )


class TestKCIDBCheckoutAPINoGroup(TestKCIDBCheckoutAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. No group."""

    anonymous = False
    groups = []


class TestKCIDBCheckoutAPIReadGroup(TestKCIDBCheckoutAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. Read group."""

    anonymous = False
    groups = ['group_a']


class TestKCIDBCheckoutAPIWriteGroup(TestKCIDBCheckoutAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. Write group."""

    anonymous = False
    groups = ['group_b']


class TestKCIDBCheckoutAPIAllGroups(TestKCIDBCheckoutAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
