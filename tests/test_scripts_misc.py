"""Test the scripts.misc module."""
from unittest import mock

from django.test.utils import override_settings
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.scripts import misc
from tests import utils


@override_settings(CELERY_TASK_ALWAYS_EAGER=True)
class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""
    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/scripts_misc.yaml',
    ]

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.utils.MSG_QUEUE.bulk_add')
    def test_send_kcidb_object_for_retriage(self, bulk_add):
        """Test send_kcidb_object_for_retriage."""

        checkout_1 = models.KCIDBCheckout.objects.get(id='rev1')

        issue_regex_1 = models.IssueRegex.objects.get(pk=1)
        issue_regex_2 = models.IssueRegex.objects.get(pk=2)

        build_1 = models.KCIDBBuild.objects.get(id='redhat:build-1')

        test_1 = models.KCIDBTest.objects.get(id='redhat:test-1')
        test_2 = models.KCIDBTest.objects.get(id='redhat:test-2')

        misc.send_kcidb_object_for_retriage([
            {'issueregex_id': 999},  # Missing id
        ])
        self.assertFalse(bulk_add.called)

        misc.send_kcidb_object_for_retriage([
            {'issueregex_id': issue_regex_1.id},
            {'issueregex_id': issue_regex_2.id},
            {'issueregex_id': issue_regex_1.id},
            {'issueregex_id': 999},  # Missing id
        ])
        bulk_add.assert_has_calls([
            mock.call(models.ObjectStatusEnum.NEEDS_TRIAGE, 'checkout', [
                serializers.KCIDBCheckoutSerializer(checkout_1).data,
            ], {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]}),
            mock.call(models.ObjectStatusEnum.NEEDS_TRIAGE, 'build', [
                serializers.KCIDBBuildSerializer(build_1).data,
            ], {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]}),
            mock.call(models.ObjectStatusEnum.NEEDS_TRIAGE, 'test', [
                serializers.KCIDBTestSerializer(test_2).data,
                serializers.KCIDBTestSerializer(test_1).data,
            ], {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]}),
        ])

    @mock.patch("datawarehouse.signals.kcidb_object")
    def test_signalize_kcidb_object(self, mocked_signal):
        """Test signalize_kcidb_object triggers the 'kcidb_object' signal as expected."""

        cases = {
            'checkout': models.KCIDBCheckout.objects.get(id='rev1'),
            'build': models.KCIDBBuild.objects.get(id='redhat:build-1'),
            'test': models.KCIDBTest.objects.get(id='redhat:test-1')
        }
        for object_type, obj in cases.items():
            with self.subTest(object_type=object_type):
                common_args = {
                    "sender": "kcidb.checkout.created_object_send_message",
                    "status": models.ObjectStatusEnum.NEW,
                    "object_type": object_type,
                    "misc": {"extra": "ketchup"},
                }
                misc.signalize_kcidb_object(**common_args, pks=[obj.iid])

                mocked_signal.send.assert_called_once_with(**common_args, objects=[obj])

                mocked_signal.reset_mock()

        invalid_object_type = 'testresult'
        with self.subTest("Unexpected object_type value should log error", object_type=invalid_object_type):
            with self.assertLogs(logger=misc.LOGGER, level="ERROR") as log_ctx:
                misc.signalize_kcidb_object(
                    sender='kcidb.checkout.created_object_send_message',
                    status=models.ObjectStatusEnum.NEW,
                    object_type=invalid_object_type,
                    pks=[models.KCIDBTestResult.objects.get(id='redhat:test-1.1').iid]
                )

            expected_log = (
                f"ERROR:{misc.LOGGER.name}:"
                f"Tried signaling unexpected object_type={invalid_object_type!r}."
            )
            self.assertIn(expected_log, log_ctx.output)

            mocked_signal.send.assert_not_called()
