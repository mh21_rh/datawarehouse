# Generated by Django 3.1.5 on 2021-02-03 12:54

from django.db import migrations, models
import django.db.models.deletion


def populate_kcidb_reports(apps, schema_editor):
    # pylint: disable=invalid-name
    """Link existing reports to KCIDBRevisions."""
    db_alias = schema_editor.connection.alias

    Report = apps.get_model('datawarehouse', 'Report')

    reports = (
        Report.objects.using(db_alias).all()
        .prefetch_related('pipeline', 'pipeline__gitlabjob', 'pipeline__gitlabjob__kcidb_revision')
    )

    for report in reports:
        job = (
            report.pipeline.gitlabjob_set
            .exclude(kcidb_revision=None)
            .first()
        )
        if not job:
            report.delete()
            continue

        report.kcidb_revision = job.kcidb_revision
        report.save()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0022_report_kcidb_revision'),
    ]

    operations = [
        migrations.RunPython(populate_kcidb_reports),
    ]
