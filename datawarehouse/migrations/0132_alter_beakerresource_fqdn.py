from django.db import migrations
from django.db import models


def fwd_remove_null(apps, schema_editor):
    """Remove BeakerResource with fqdn=null and update KCIDBTest pointing to it."""
    BeakerResource = apps.get_model('datawarehouse', 'BeakerResource')
    KCIDBTest = apps.get_model('datawarehouse', 'KCIDBTest')

    db_alias = schema_editor.connection.alias

    if null_fqdn := BeakerResource.objects.using(db_alias).filter(fqdn=None).first():
        KCIDBTest.objects.using(db_alias).filter(environment=null_fqdn).update(environment=None)
        null_fqdn.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0131_provenancecomponent_misc'),
    ]

    # Run without a PostgreSQL transaction to commit cleaning before trying to alter table
    atomic = False

    operations = [
        migrations.RunPython(fwd_remove_null, migrations.RunPython.noop),
        migrations.AlterField(
            model_name='beakerresource',
            name='fqdn',
            field=models.CharField(blank=True, max_length=100, unique=True),
        ),
    ]
