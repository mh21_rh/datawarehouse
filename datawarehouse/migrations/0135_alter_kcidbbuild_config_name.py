# Generated by Django 4.1.13 on 2024-04-22 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0134_queuedtask_unique_call_id_when_start_time_isnull'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kcidbbuild',
            name='config_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
