# Generated by Django 5.0.7 on 2024-08-02 15:17

import django.db.models.deletion

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('datawarehouse', '0139_support_riscv'),
    ]

    operations = [
        # Make group nullable to allow reverting the migration
        migrations.AlterField(
            model_name='ldapgrouplink',
            name='group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='saml_link', to='auth.group'),
        ),
        migrations.AddField(
            model_name='ldapgrouplink',
            name='description',
            field=models.CharField(max_length=None, blank=True, null=True),
        ),
        migrations.AddField(
            model_name='ldapgrouplink',
            name='groups',
            field=models.ManyToManyField(related_name='ldap_group_link_set', to='auth.group'),
        ),
    ]
