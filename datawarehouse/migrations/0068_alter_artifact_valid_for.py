# Generated by Django 3.2.6 on 2021-08-27 10:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0067_alter_compiler_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artifact',
            name='valid_for',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
