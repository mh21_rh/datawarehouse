from django.db import migrations, models


def create_policies(apps, schema_editor):
    """Populate default policies."""
    db_alias = schema_editor.connection.alias
    Policy = apps.get_model("datawarehouse", "Policy")

    # Add default public policy
    Policy.objects.using(db_alias).create(name='public')

class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0031_issue_policy'),
    ]

    operations = [
        migrations.RunPython(create_policies)
    ]
