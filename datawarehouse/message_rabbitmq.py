"""RabbitMQ messaging implementation."""
from cki_lib import messagequeue
from cki_lib.logger import get_logger
from django.conf import settings
from django.db import transaction
from django.utils import timezone
import pika
import prometheus_client

from datawarehouse import models

LOGGER = get_logger(__name__)

MESSAGES_SENT = prometheus_client.Counter(
    'cki_dw_rabbitmq_messages_sent',
    'Count of sent messages',
    ['object_type', 'status']
)


MESSAGES_ERROR = prometheus_client.Counter(
    'cki_dw_rabbitmq_messages_error',
    'Count of messages that could not be sent',
)


class MessageQueue:
    """Messages Queue."""

    def __init__(self):
        # pylint: disable=too-many-arguments
        """Init."""
        self.queue = messagequeue.MessageQueue(keepalive_s=settings.RABBITMQ_KEEPALIVE_S)

    def send(self):
        """Send messages in the queue."""
        messages = (
            models.MessagePending.objects
            .select_for_update(skip_locked=True)
            .filter(kind=models.MessageKindEnum.RABBITMQ)
        )
        with transaction.atomic():
            for message in messages:
                object_type = message.content['object_type']
                status = message.content['status']
                priority = 0 if status == models.ObjectStatusEnum.NEEDS_TRIAGE else 1
                try:
                    self.queue.send_message(
                        message.content,
                        f'datawarehouse.{object_type}.{status}',
                        exchange=settings.RABBITMQ_EXCHANGE_KCIDB_NOTIFICATIONS,
                        headers={'message-type': 'datawarehouse'},
                        priority=priority,
                    )
                    MESSAGES_SENT.labels(object_type, status).inc()
                except pika.exceptions.AMQPError:
                    LOGGER.exception('Error sending message, will be retried')
                    MESSAGES_ERROR.inc()
                    break
                else:
                    message.delete()

    @staticmethod
    def bulk_add(status, object_type, content, misc):
        """Bulk add a list of kcidb messages to the outgoing queue."""
        if not settings.RABBITMQ_SEND_ENABLED:
            LOGGER.debug("KCIDB notification skipped. No RabbitMQ server configured.")
            return

        models.MessagePending.objects.bulk_create([models.MessagePending(
            kind=models.MessageKindEnum.RABBITMQ,
            content={
                'timestamp': timezone.now().isoformat(),
                'status': status,
                'object_type': object_type,
                'misc': misc or {},
                'object': c,
            },
        ) for c in content])
