"""Style templatetags."""
from django.template import Library

from datawarehouse import styles

register = Library()


@register.filter
def get_style(result, actionable=True):
    """Get style format for a given result."""
    if result is True:
        result = 'P'
    elif result is False:
        result = 'F'

    return styles.get_style(result, not actionable)


@register.filter
def get_test_style(test):
    """Get test style format."""
    non_blocking = not test.is_missing_triage
    return styles.get_style(test.status, non_blocking)


@register.filter
def get_status_text(status):
    """Get human readable text of status."""
    status_dict = {
        'F': 'Failed',
        'E': 'Errored',
        'M': 'Missed',
        'P': 'Passed',
        'D': 'Done',
        'S': 'Skipped',
        'N': 'New',
    }

    if not status:
        return "Running"

    return status_dict.get(status, "Unknown")
