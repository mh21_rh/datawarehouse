"""Run cronjobs from CLI."""
from cki_lib import cronjob
from django.core.management.base import BaseCommand

from datawarehouse.cron import JOBS


def run():
    """Loop and update metrics."""
    cronjob.run(JOBS)


class Command(BaseCommand):
    """Django manage command."""

    help = 'Run cron jobs in a loop'

    def add_arguments(self, parser):
        """Add command arguments."""
        parser.add_argument('action', choices=['run'], help='Action to run')

    def handle(self, *args, **options):
        """Command entrypoint."""
        if options['action'] == 'run':
            run()
