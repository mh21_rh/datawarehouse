"""Policy scripts."""
from collections import defaultdict

from django.conf import settings
from django.contrib.auth import get_user_model

from datawarehouse import ldap
from datawarehouse import models

User = get_user_model()


def update_issue_policy(issues_ids):
    """
    Update the Issue policy after a new ocurrence is assigned.

    When a post_add signal is received, check if the issue has to be
    updated to public depending on the related objects.
    If an Issue is set to something different to PUBLIC, but it has
    at least one related checkout that is public, set policy to PUBLIC.
    """
    public_policy = models.Policy.objects.get(name=models.Policy.PUBLIC)

    models.Issue.objects.filter(
        id__in=issues_ids,
        # Only the ones with policy_auto_public
        policy_auto_public=True,
        # Only the ones that have a public tree tagged
        issueoccurrence__related_checkout__policy=public_policy,
    ).exclude(
        # Exclude the ones already public
        policy=public_policy
    ).update(
        policy=public_policy
    )


def update_ldap_group_members(ldap_group_link=None):
    """Update Group members based on LDAPGroupLink."""
    if not settings.FF_LDAP_GROUP_SYNC:
        return

    groups = defaultdict(list)

    ldap_group_links = models.LDAPGroupLink.objects.exclude(groups=None).all()
    if ldap_group_link:
        ldap_group_links = ldap_group_links.filter(id=ldap_group_link.id)

    with ldap.connection() as conn:
        for link in ldap_group_links:
            if ldap_users := conn.get_users(link.filter_query):
                users = User.objects.raw(
                    'SELECT "id" FROM "auth_user" WHERE ("username", "email") IN %s', [tuple(ldap_users)]
                )
                for group in link.groups.all():
                    groups[group].extend(users)

            if link.extra_users.exists():
                for group in link.groups.all():
                    groups[group].extend(link.extra_users.all())

    for group, users in groups.items():
        if ldap_group_link:
            group.user_set.add(*users)
        else:
            group.user_set.set(users)


def update_ldap_group_members_for_user(user):
    """
    Update the LDAP groups for a single user.

    This function is different from update_ldap_group_members mainly because it
    does not rely on the user being present on the database, but uses the <User>
    instance passed as an argument.
    """
    if not settings.FF_LDAP_GROUP_SYNC:
        return

    with ldap.connection() as conn:
        for link in models.LDAPGroupLink.objects.all():
            user_is_member = any(
                user.email == email
                for _, email in conn.get_users(link.filter_query)
            )
            if user_is_member:
                user.groups.add(*link.groups.values_list("id", flat=True))
