"""Styles file."""
# pylint: disable=too-few-public-methods

from dataclasses import dataclass

from datawarehouse import models


@dataclass
class Style:  # pylint: disable=R0902
    """Encapsulates GUI styles related to task results."""

    icon: str
    icon_big: str
    cell: str
    row: str
    color: str
    text_color: str
    cell_with_text_color: str
    status_class: str

    def __init__(self, icon: str, color: str = '', status_class: str = '', colored: bool = False) -> None:
        """Construct a new Style class."""
        table_color = f'table-{color}' if color else ''
        self.text_color = f'text-{color}' if color else ''
        self.icon = f'<i class="{icon} {self.text_color}"></i>'
        self.icon_big = f'<i class="{icon} {self.text_color} fa-2x"></i>'
        self.cell = f'col-1 {table_color}'
        self.row = table_color if colored else ''
        self.color = color
        self.cell_with_text_color = f'{self.cell} {self.text_color}'
        self.status_class = f'{status_class} colored-bg' if colored else status_class
        self.icon_name = icon

    def __str__(self):
        """Return status_class as the str representation."""
        return self.status_class


_STYLE_RUNNING = Style('fas fa-sync', 'info', status_class='status-running')
_STYLE_SKIP = Style('far fa-window-minimize', status_class='status-skip')
_STYLE_PASS = Style('fas fa-check', 'success', status_class='status-pass')
_STYLE_MISS = Style('far fa-circle', 'warning', status_class='status-miss')
_STYLE_FAIL = Style('fas fa-times', 'danger', status_class='status-fail', colored=True)
_STYLE_FAIL_WAIVED = Style('fas fa-times', 'danger', status_class='status-fail')
_STYLE_ERROR = Style('fas fa-fire-alt', 'danger', status_class='status-error', colored=True)
_STYLE_ERROR_WAIVED = Style('fas fa-fire-alt', 'warning', status_class='status-error')
_STYLE_DEFAULT = Style('fas fa-times', 'danger', status_class='status-fail', colored=True)


def get_style(level: str, waived: bool = False) -> Style:
    """Return style values for template rendering."""
    style = _STYLE_DEFAULT
    if level == 'Running' or level is None:
        style = _STYLE_RUNNING

    elif level == models.ResultEnum.MISS:
        style = _STYLE_MISS

    elif level == models.ResultEnum.SKIP:
        style = _STYLE_SKIP

    elif level == models.ResultEnum.PASS:
        style = _STYLE_PASS

    elif level == models.ResultEnum.ERROR:
        style = _STYLE_ERROR_WAIVED if waived else _STYLE_ERROR

    elif level == models.ResultEnum.FAIL:
        style = _STYLE_FAIL_WAIVED if waived else _STYLE_FAIL

    return style
