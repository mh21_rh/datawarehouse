---
title: KCIDB - Test
---

## Get

Get a single test.

`GET /api/1/kcidb/tests/$test_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `test_id` | `str/int` | Yes | `id` or `iid` of the test to get. |

Example of response:

```json
{
    "build_id": "redhat:926214",
    "id": "redhat:112448273",
    "origin": "redhat",
    "environment": {
        "description": "hpe-bl280cg6-01.hpe2.lab.eng.bos.redhat.com"
    },
    "path": "blktests",
    "description": "Storage blktests",
    "status": "PASS",
    "waived": true,
    "start_time": "2020-06-30T19:21:33Z",
    "duration": 739,
    "output_files": [
        {
            "url": "https://s3.url/datawarehouse/609265/build_x86_64/tests/Storage_blktests/x86_64_3_harness.log",
            "name": "x86_64_3_harness.log"
        },
        ...
    ],
    "misc": {
        "iid": 10180
    }
}
```

## List

Get a list of tests for a build.

`GET /api/1/kcidb/builds/$build_id/tests`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `checkout_id` | `str/int` | Yes | `id` or `iid` of the tests' checkout. |
| `build_id` | `str/int` | Yes | `id` or `iid` of the tests' build. |

Example of response:

```json

{
    "count": 51,
    "next": "http://server/api/1/kcidb/checkouts/b8fba93561c984f336d47d6d544be3a2a920bac3/builds/390/tests?limit=30&offset=30",
    "previous": null,
    "results": [
        {
            "build_id": "redhat:926214",
            "id": "redhat:112448273",
            "origin": "redhat",
            "environment": {
                "description": "hpe-bl280cg6-01.hpe2.lab.eng.bos.redhat.com"
            },
            "path": "blktests",
            "description": "Storage blktests",
            "status": "PASS",
            "waived": true,
            "start_time": "2020-06-30T19:21:33Z",
            "duration": 739,
            "output_files": [
                {
                    "url": "https://s3.url/datawarehouse/609265/build_x86_64/tests/Storage_blktests/x86_64_3_harness.log",
                    "name": "x86_64_3_harness.log"
                },
                ...
            ],
            "misc": {
                "iid": 10180
            }
        },
        ...
    ]
}
```

## Get Test Result

Get a single test result.

`GET /api/1/kcidb/testresults/$testresult_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `testresult_id` | `str/int` | Yes | `id` or `iid` of the test result to get. |

Example of response:

```json
{
    "id": "redhat:123456_x86_64_kernel_distribution_ltp_lite.1",
    "name": "install",
    "status": "PASS",
    "output_files": [
        {
            "url": "https://url/recipes/11676474/tasks/141708760/results/661842966/logs/avc.log",
            "name": "avc.log"
        },
        {
            "url": "https://url/recipes/11676474/tasks/141708760/results/661842966/logs/resultoutputfile.log",
            "name": "resultoutputfile.log"
        }
    ],
    "misc": {
        "iid": 438
    }
}
```

## List Test Results

Get a list of test results for a test.

`GET /api/1/kcidb/tests/$test_id/results`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `test_id` | `str/int` | Yes | `id` or `iid` of the test. |

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": "redhat:123456_x86_64_kernel_distribution_ltp_lite.1",
            "name": "install",
            "status": "PASS",
            "output_files": [
                {
                    "url": "https://url/recipes/11676474/tasks/141708760/results/661842966/logs/avc.log",
                    "name": "avc.log"
                },
                {
                    "url": "https://url/recipes/11676474/tasks/141708760/results/661842966/logs/resultoutputfile.log",
                    "name": "resultoutputfile.log"
                }
            ],
            "misc": {
                "iid": 438
            }
        }
    ]
}
```
