---
title: DataWarehouse Authorization and Permission
linkTitle: Authorization and Permission
description: Overview of DataWarehouse access control and user permissions
weight: 30
aliases: [/l/datawarehouse-permissions, /l/datawarehouse-auth-and-perms]
---

{{% alert title="TL;DR" color="info" %}}

- There are 3 visibility (`Policy`) levels: `public`, `internal`, `retrigger`;
- Objects (`Issue` and `KCIDBCheckout` + children) visibility is defined by the `Policy` they are
  created with;
- Admins grant permissions to Users using DW Groups; preferably through LDAP group links;
- DW Groups required to triage internal objects: `Triagers`, `policy_public_write`,
  `policy_internal_read`, `policy_internal_write`.
- CKI team members can be assigned to the DW Group `CKI Team`, or be enabled as a _superuser_.

{{% /alert %}}

[DataWarehouse] uses Django [Auth Module] with **Users**, **Groups** and **Permissions**
that limit operations. On top of that, we have **object-level permission**,
combining Groups with our custom [PolicyAuthorizationBackend] to distinguish between
**Public** and **Internal** (Red Hat) data, and also data from **retriggered** pipelines.

## Creating an Account

If you are a Red Hat associate, your account will be created once you have
logged in successfully via OIDC for the first time. Otherwise, accounts are
currently managed by the CKI team, reachable through [cki-project@redhat.com],
Red Hat associates can also ping us in the internal Slack channel: `#team-kernel-cki`.

## Consuming the API

The API follows the same permission policies from standard website usage,
which will be explained below, other than that,
see [API auth] documentation for information regarding out-of-browser authentication.

## Permission Groups

### Policies

There are 3 levels of visibility, in DataWarehouse: `public`, `internal` and `retrigger`,
each with a DW group associated with `read` and `write` permissions.
The policies apply to `KCIDBCheckouts` and `Issues`, and are propagated to their children,
i.e. `KCIDBBuild`, `KCIDBTest`, `KCIDBTestResults`, `Artifacts`, `IssueOccurrences`,
and `IssueRegexes`:

1. **public:** `(read=None,write="policy_public_write")`

   Public data allows read operations (GET requests) without authentication,
   while write operations (POST, PUT, DELETE requests), e.g. who can submit results,
   require basic permissions, which are granted to members of  the `policy_public_write` DW group.

2. **internal:**`(read="policy_internal_read",write="policy_internal_write")`

   Certain records pertaining to Red Hat data are restricted to internal users
   with appropriate read and write permissions, which are granted to members of
   `policy_internal_read` and `policy_internal_write`, respectively.

3. **retrigger:**`(read="policy_retrigger_rw",write="policy_retrigger_rw")`

   Users in the `policy_retrigger_rw` DW group can access and update objects with this policy.
   This data has a debug nature, and is submitted by the [cki-ci-bot's pipelines][pipeline-bot],
   triggered when testing merge requests.
   This policy is used only by service-accounts, however superusers have the same privileges.

{{% alert color="secondary" %}}
Ensure you are authenticated and have the necessary permissions before
attempting to access restricted records.
You can review which DW groups you are a member of in your DataWarehouse profile at
<https://datawarehouse.cki-project.org/user/USERNAME>
{{% /alert %}}

If you are a DataWarehouse contributor, you might want to know that the
implementation is based on **Django Rest Framework** framework of [custom permissions],
which we've used to define our [PolicyAuthorizationPermission] and [PolicyAuthorizationBackend],
then applied it to most models' default queryset subclassing [AuthorizedQuerySet].

### Triaging permissions for QE Teams

To enable an user to manipulate issues, their occurrences and regexes,
in addition to the relevant  object-level _read/write policy_ groups,
they need to be part of the `Triagers` DW group, which will grant them permission
to read/write operations on those objects.

{{% alert title="🧠" color="info" %}}

1. `Triagers`: Grant permission to add, update and delete issues, occurrences and regexes.
2. `policy_public_write`: Grant permission to create, update and delete public data.
3. `policy_internal_read`: Grant permission to list and read public data.
4. `policy_internal_write`: Grant permission to create, update and delete internal data.

{{% /alert %}}

## How to be a member of a DW group?

DataWarehouse admins don't manage DW groups directly,
instead they manage [admin/LDAP-group-link][admin-ldapgrouplink],
where they assign DW groups:

* to service-accounts and external users listed as "extra users" in the LDAP-group-link.
* to users who match an LDAP/Rover query (for Red Hat associates).
  The LDAP sync happens when the query is created or updated,
  but also in an hourly cronjob ([`UpdateLdapGroupMembers`][dw-cronjobs]).

### LDAP/Rover Group Sync

Nowadays, we have been using a team-based permission management, granting triaging permissions to
all members of interested QE teams, creating [LDAP group link][admin-ldapgrouplink] for the four
groups mentioned above. Currently, these teams are enabled for this:

- [Desktop QE (desktop-qe)](https://rover.redhat.com/groups/group/desktop-qe)
- [Kernel QE (kernel-qe)](https://rover.redhat.com/groups/group/kernel-qe)
- [Kernel SE (rhel-sst-se-kernel)](https://rover.redhat.com/groups/group/rhel-sst-se-kernel)
- [Linux QE (linux-qe)](https://rover.redhat.com/groups/group/linux-qe)
- [Network QE (network-qe)](https://rover.redhat.com/groups/group/network-qe)

But there are still some users managed manually in these Rover groups:

| Rover Group                        | DW Groups                                                     |
| ---------------------------------- | ------------------------------------------------------------- |
| [cki-datawarehouse-public-write]   | `public-write`                                                |
| [cki-datawarehouse-internal-write] | `internal-write`                                              |
| [linux-eng-pe]                     | `internal-read`                                               |
| [bugzilla-redhat]                  | `internal-read`                                               |
| [cki-kernel-tests-reviewers] [^1]  | `public-write`, `internal-read`, `internal-write`, `Triagers` |

[^1]: **NOTE**: members of [cki-kernel-tests-reviewers] will be automated in
      <https://gitlab.com/cki-project/datawarehouse/-/issues/218>

{{% alert title="Enabling triaging for a new QE Team" color="info" %}}

To grant permission to a new team:

1. Get the LDAP query from their Rover page
2. Add a record of LDAP group link, pointing to the 4 DW Groups:
   `Triagers`, `public-write`, `internal-write`, `internal-write`.

{{% /alert %}}

[cki-project@redhat.com]: mailto:cki-project@redhat.com
[DataWarehouse]: https://datawarehouse.cki-project.org/
[admin-ldapgrouplink]: https://datawarehouse.cki-project.org/admin/datawarehouse/ldapgrouplink/
[Auth Module]: https://docs.djangoproject.com/en/5.0/topics/auth/default
[custom permissions]: https://www.django-rest-framework.org/api-guide/permissions/#custom-permissions
[PolicyAuthorizationBackend]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/authorization.py
[PolicyAuthorizationPermission]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/api/permissions.py
[AuthorizedQuerySet]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/models/utils.py
[dw-cronjobs]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/cron/jobs.py
[cki-datawarehouse-public-write]: https://rover.redhat.com/groups/group/cki-datawarehouse-public-write
[cki-datawarehouse-internal-write]: https://rover.redhat.com/groups/group/cki-datawarehouse-internal-write
[linux-eng-pe]: https://rover.redhat.com/groups/group/linux-eng-pe
[bugzilla-redhat]: https://rover.redhat.com/groups/group/bugzilla-redhat
[cki-kernel-tests-reviewers]: https://rover.redhat.com/groups/group/cki-kernel-tests-reviewers
[API auth]: api/auth.md
[pipeline-bot]: /l/pipeline-bot
